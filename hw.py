from src.airfleet import *
from src.logger import *


def main():
	__logger = Logger()

	airFleets = [S7(), Utair(), Rossiya(), Nordstar(), Ural()]
	for airFleet in airFleets:
		__logger.cmd(airFleet.companyName + ':', 2)
		print(airFleet.aboutAircrafts)
		print('\n')


if __name__ == '__main__':
	main()
